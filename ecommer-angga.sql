-- phpMyAdmin SQL Dump
-- version 4.8.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 14, 2018 at 05:58 AM
-- Server version: 10.1.32-MariaDB
-- PHP Version: 7.2.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ecommer-angga`
--

-- --------------------------------------------------------

--
-- Table structure for table `groups`
--

CREATE TABLE `groups` (
  `id` tinyint(1) NOT NULL,
  `name` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `groups`
--

INSERT INTO `groups` (`id`, `name`) VALUES
(1, 'Admin'),
(2, 'Member');

-- --------------------------------------------------------

--
-- Table structure for table `invoices`
--

CREATE TABLE `invoices` (
  `id` int(10) NOT NULL,
  `date` datetime NOT NULL,
  `due_date` datetime NOT NULL,
  `status` enum('paid','unpaid','canceled','expired') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `invoices`
--

INSERT INTO `invoices` (`id`, `date`, `due_date`, `status`) VALUES
(13, '2018-07-14 05:55:21', '2018-07-15 05:55:21', 'unpaid'),
(14, '2018-07-14 05:55:40', '2018-07-15 05:55:40', 'unpaid'),
(15, '2018-07-14 05:57:27', '2018-07-15 05:57:27', 'unpaid');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `version` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`version`) VALUES
(5);

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` int(10) NOT NULL,
  `invoice_id` int(10) NOT NULL,
  `product_id` int(10) NOT NULL,
  `product_name` varchar(50) NOT NULL,
  `qty` int(3) NOT NULL,
  `price` int(9) NOT NULL,
  `options` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `invoice_id`, `product_id`, `product_name`, `qty`, `price`, `options`) VALUES
(1, 1, 1, 'Baju', 1, 79000, ''),
(2, 1, 3, 'Topi', 1, 80000, ''),
(3, 2, 5, 'Dasi', 1, 77500, ''),
(4, 3, 1, 'Baju', 1, 79000, ''),
(5, 3, 2, 'Sandal Casual', 1, 35000, ''),
(6, 4, 2, 'Sandal Casual', 1, 35000, ''),
(7, 5, 2, 'Sandal Casual', 1, 35000, ''),
(8, 6, 1, 'Baju', 1, 79000, ''),
(9, 6, 2, 'Sandal Casual', 1, 35000, ''),
(10, 6, 3, 'Topi', 1, 80000, ''),
(11, 6, 4, 'Sepatu', 1, 240000, ''),
(12, 6, 5, 'Dasi', 1, 77500, ''),
(13, 6, 6, 'Celana Panjang', 1, 200000, ''),
(14, 6, 7, 'Celana Pendek', 1, 150000, ''),
(15, 6, 8, 'Sepatu Futsal', 1, 350000, ''),
(16, 7, 1, 'Baju', 1, 79000, ''),
(17, 7, 2, 'Sandal Casual', 1, 35000, ''),
(18, 7, 3, 'Topi', 1, 80000, ''),
(19, 7, 8, 'Sepatu Futsal', 1, 350000, ''),
(20, 8, 1, 'Baju', 1, 79000, ''),
(21, 8, 2, 'Sandal Casual', 1, 35000, ''),
(22, 10, 1, 'Baju', 1, 79000, ''),
(23, 10, 2, 'Sandal Casual', 1, 35000, ''),
(24, 10, 3, 'Topi', 1, 80000, ''),
(25, 10, 4, 'Kaos', 1, 50000, ''),
(26, 11, 1, 'Baju', 1, 79000, ''),
(27, 11, 2, 'Sandal Casual', 1, 35000, ''),
(28, 11, 3, 'Topi', 1, 80000, ''),
(29, 12, 1, 'Baju', 1, 79000, ''),
(30, 12, 2, 'Sandal Casual', 1, 35000, ''),
(31, 13, 3, 'Parodia', 1, 60000, ''),
(32, 13, 2, 'Ariocarpus', 1, 45000, ''),
(33, 13, 4, 'Echinofossulocactus', 1, 50000, ''),
(34, 14, 1, 'Kaktus Ball', 1, 79000, ''),
(35, 14, 3, 'Parodia', 1, 60000, ''),
(36, 14, 2, 'Ariocarpus', 1, 45000, ''),
(37, 15, 2, 'Ariocarpus', 1, 45000, ''),
(38, 15, 1, 'Kaktus Ball', 1, 79000, ''),
(39, 15, 3, 'Parodia', 1, 60000, ''),
(40, 15, 4, 'Echinofossulocactus', 1, 50000, ''),
(41, 15, 7, 'Echinocactus', 1, 70000, '');

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(10) NOT NULL,
  `name` varchar(50) NOT NULL,
  `description` text NOT NULL,
  `price` int(9) NOT NULL,
  `stock` int(3) NOT NULL,
  `image` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `name`, `description`, `price`, `stock`, `image`) VALUES
(1, 'Kaktus Ball', '[Kaktus asal Perbas]', 79000, 5, 'kaktus-ball.jpg'),
(2, 'Ariocarpus', '[Kaktus asal Belgia]', 45000, 10, '729979-kaktus-hias-untuk-rumah-.jpg'),
(3, 'Parodia', '[Kaktus asal walanda]', 60000, 12, '729980-kaktus-hias-untuk-rumah-.jpg'),
(4, 'Echinofossulocactus', '[Kaktus asal japan]', 50000, 5, '729982-kaktus-hias-untuk-rumah-.jpg'),
(5, 'Gymnacalycium ', '[Kaktus asal Soreang]', 35000, 5, '729983-kaktus-hias-untuk-rumah-.jpg'),
(6, 'Lobivia Oganmaru', '[Kaktus asal Pacet]', 50000, 5, '729985-kaktus-hias-untuk-rumah-.jpg'),
(7, 'Echinocactus', '[Kaktus asal Papua]', 70000, 4, '729987-1000xauto-kaktus-hias-untuk-rumah-.jpg'),
(8, 'Grusonii', '[Kaktus asal Korean]', 35000, 7, 'KaktusEchinocactusGrusonii-kaktus-mahal-indonesia-kaktus-hias.jpg'),
(9, 'Hawortia ', '[Kaktus asal Uruguay]', 51000, 6, 'haworthia.jpg'),
(10, 'Xrocutia', '[Kaktus asal Costarika]', 45000, 5, 'index.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `toko_sessions`
--

CREATE TABLE `toko_sessions` (
  `session_id` varchar(40) NOT NULL DEFAULT '0',
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `user_agent` varchar(120) NOT NULL,
  `last_activity` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `user_data` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `toko_sessions`
--

INSERT INTO `toko_sessions` (`session_id`, `ip_address`, `user_agent`, `last_activity`, `user_data`) VALUES
('6d30d9246cd72e81b9012acb952a59f3', '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:60.0) Gecko/20100101 Firefox/60.0', 1531534046, ''),
('ddda97774a764400f94b35614d964752', '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:60.0) Gecko/20100101 Firefox/60.0', 1531540672, '');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) NOT NULL,
  `username` varchar(25) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(60) NOT NULL,
  `group` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `email`, `password`, `group`) VALUES
(1, 'admin', 'admin@gmail.com', 'admin', 1),
(14, 'angga', 'angga@gmail.com', 'angga', 2),
(15, '0', '0', '0', 0),
(16, 'xeno', 'xeno@gmail.com', 'xeno', 2);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `groups`
--
ALTER TABLE `groups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `invoices`
--
ALTER TABLE `invoices`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `toko_sessions`
--
ALTER TABLE `toko_sessions`
  ADD PRIMARY KEY (`session_id`),
  ADD KEY `last_activity_idx` (`last_activity`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `groups`
--
ALTER TABLE `groups`
  MODIFY `id` tinyint(1) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `invoices`
--
ALTER TABLE `invoices`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
